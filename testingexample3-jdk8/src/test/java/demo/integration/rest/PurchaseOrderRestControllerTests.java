package demo.integration.rest;

import org.joda.time.LocalDate;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.hateoas.Link;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;

import demo.Application;
import demo.integration.rest.dto.PlantResource;
import demo.integration.rest.dto.PlantResourceAssembler;
import demo.integration.rest.dto.PurchaseOrderResource;
import demo.models.PlantRepository;
import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
    DbUnitTestExecutionListener.class })
public class PurchaseOrderRestControllerTests {
    @Autowired
    private WebApplicationContext wac;
    
    private MockMvc mockMvc;
    private ObjectMapper mapper = new ObjectMapper();

    @Before
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
    }
	
	@Test
    @DatabaseSetup(value="dataset.xml")
	public void testCreatePO() throws Exception {
		PlantResource plant = new PlantResource();
		plant.add(new Link("http://localhost:8080/rest/plants/10001"));
		PurchaseOrderResource po = new PurchaseOrderResource();
		po.setPlant(plant);
		po.setStartDate(new LocalDate().toDate());
		po.setEndDate(new LocalDate().toDate());
		
		mockMvc.perform(post("/rest/pos")
			.contentType(MediaType.APPLICATION_JSON)
			.content(mapper.writeValueAsString(po))
		)
			.andExpect(status().isCreated());
	}
}
