package demo.models;

import java.util.Date;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface PlantRepository extends JpaRepository<Plant, Long> {

	@Query("select case when count(po) > 0 then false else true end " +
			"from PurchaseOrder po " +
			"where po.plant = :plant and " +
			"(po.startDate between :startDate and :endDate or po.endDate between :startDate and :endDate)")
	boolean isAvailable(@Param("plant") Plant plant, @Param("startDate") Date startDate, @Param("endDate") Date endDate);
}