package demo.integration.rest.dto;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;
import demo.util.ResourceSupport;

@Getter
@Setter
public class PurchaseOrderResource extends ResourceSupport {
    PlantResource plant;
    Date startDate;
    Date endDate;
    Float cost;
}
