package demo.integration.rest.dto;

import org.springframework.hateoas.mvc.ResourceAssemblerSupport;

import demo.integration.rest.PurchaseOrderRestController;
import demo.models.PurchaseOrder;

public class PurchaseOrderResourceAssembler extends ResourceAssemblerSupport<PurchaseOrder, PurchaseOrderResource>{

	static PlantResourceAssembler plantRA = new PlantResourceAssembler();
	
	public PurchaseOrderResourceAssembler() {
		super(PurchaseOrderRestController.class, PurchaseOrderResource.class);
	}

	@Override
	public PurchaseOrderResource toResource(PurchaseOrder po) {
		PurchaseOrderResource poRS = createResourceWithId(po.getId(), po);
		po.setStartDate(po.getStartDate());
		po.setEndDate(po.getEndDate());
		po.setCost(po.getCost());
		if (po.getPlant() != null)
			poRS.setPlant(plantRA.toResource(po.getPlant()));
		return poRS;
	}

}
