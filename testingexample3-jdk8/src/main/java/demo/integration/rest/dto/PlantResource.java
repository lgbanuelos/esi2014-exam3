package demo.integration.rest.dto;

import demo.util.ResourceSupport;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PlantResource extends ResourceSupport {
    String name;
    String description;
    Float price;
}
