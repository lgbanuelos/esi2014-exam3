package demo.managers;

public class PlantUnavailableException extends Exception {
 	private static final long serialVersionUID = 5618446819816421912L;
 	public PlantUnavailableException(String msg) {
		super(msg);
	}
}
