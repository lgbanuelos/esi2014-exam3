package demo.models;

public enum POStatus {
	PENDING, REJECTED, OPEN, CLOSED, CANCELLED
}
