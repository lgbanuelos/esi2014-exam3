package demo.managers;

import org.joda.time.Days;
import org.joda.time.LocalDate;
import org.joda.time.LocalTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import demo.models.PlantRepository;
import demo.models.PurchaseOrder;
import demo.models.PurchaseOrderRepository;

@Service
public class PurchaseOrderManager {

	@Autowired
	PlantRepository plantRepo;
	
	@Autowired
	PurchaseOrderRepository poRepo;
	
	public PurchaseOrder createPO(PurchaseOrder po) throws InvalidRentalPeriodException, PlantUnavailableException {
		LocalDate _startDate = new LocalDate(po.getStartDate());
		LocalDate _endDate = new LocalDate(po.getEndDate());
		int numberOfDays = Days.daysBetween(_startDate, _endDate).getDays();
		
		System.out.println("Start date: " + _startDate.toString());
		
		if (numberOfDays < 1)
			throw new InvalidRentalPeriodException("The rental period is for zero or a negative number of days!");
		
		if (_startDate.isBefore(LocalDate.now()))
			throw new InvalidRentalPeriodException("The start day for the rental period has already passed!");
		
		if (plantRepo.isAvailable(po.getPlant(), po.getStartDate(), po.getEndDate())) {
		    po.setCost(numberOfDays * po.getPlant().getPrice());
		    poRepo.save(po);
		} else
			throw new PlantUnavailableException("The requested plant is not available");

		return po;
	}
}
