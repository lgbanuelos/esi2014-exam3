package demo.integration.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import demo.integration.rest.dto.PurchaseOrderResource;
import demo.managers.InvalidRentalPeriodException;
import demo.managers.PlantUnavailableException;
import demo.managers.PurchaseOrderManager;
import demo.models.Plant;
import demo.models.PlantRepository;
import demo.models.PurchaseOrder;
import static org.mentaregex.Regex.match;

@RestController
@RequestMapping("/rest/pos")
public class PurchaseOrderRestController {
	@Autowired
	PlantRepository plantRepo;
	@Autowired
	PurchaseOrderManager poManager;
	
	@RequestMapping(method=RequestMethod.POST, value="")
	public PurchaseOrderResource createPO(@RequestBody PurchaseOrderResource poRS) throws InvalidRentalPeriodException, PlantUnavailableException {
		long plantId = Long.parseLong(match(poRS.getPlant().getId().getHref(), "/.+\\/(\\d+)$/")[0]);
		Plant plant = plantRepo.findOne(plantId);
		PurchaseOrder po = new PurchaseOrder();
		poManager.createPO(po);
		
		return null;
	}
}
