package demo.integration.rest.dto;

import org.springframework.hateoas.mvc.ResourceAssemblerSupport;

import demo.integration.rest.PurchaseOrderRestController;
import demo.models.Plant;

public class PlantResourceAssembler extends ResourceAssemblerSupport<Plant, PlantResource>{

	public PlantResourceAssembler() {
		super(PurchaseOrderRestController.class, PlantResource.class);
	}

	@Override
	public PlantResource toResource(Plant plant) {
		PlantResource plantRS = createResourceWithId(plant.getId(), plant);
		plantRS.setDescription(plant.getDescription());
		plantRS.setName(plant.getName());
		plantRS.setPrice(plant.getPrice());
		return plantRS;
	}

}
